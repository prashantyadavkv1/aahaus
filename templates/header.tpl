<!DOCTYPE html>
<html lang=en>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="" content="" />
    <meta name="" content="" />
    <meta name="" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>All Assignment Help Australia: Assignment Writing Services @ 50% Off</title>
    <meta name="description" content=""/>
    <meta name="keywords" content="All Assignment Help Australia"/>
    <meta name="author" content="index.html" />
    <meta name="Language" content="En" />
    <meta name="Copyright" content="Copyright 2017 @ All Assignment Help Australia" />
    <meta name="Robots" content="index, follow" />
    <link rel="publisher" href="">
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="ALL ASSIGNMENT HELP" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="index.html" />
    <meta property="article:publisher" content="" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="ALL ASSIGNMENT HELP" />
    <meta name="twitter:site" content="" />
    <meta name="twitter:creator" content="" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="icon" href="https://www.allassignmenthelp.com.au/static/images/favicon.png" sizes="16x16"/>
    <link rel="shortcut icon" href="https://www.allassignmenthelp.com.au/static/images/favicon.ico"/>
    <link rel="canonical" href="https://www.allassignmenthelp.com.au/"/>

    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="~$cssUrl`/custom.css" rel="stylesheet" type="text/css" />
</head>
<body>
