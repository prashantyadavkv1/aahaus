<div class="header_part">
    <div class="top-strip">
        <div class="container">
            <div class="top-msg">Toll Free: +61 361 216 125 </div>
            <div class="phone-details">
                <ul>
                    <li class="refer" style="display:none"> <a href="#">Refer a Friends </a></li>
                    <li> <a href="javascript:void(0)" class="phoneSupport" id="phoneSupport"><i class="fa fa-phone"></i> <span class="navHeadChatText">Phone Support</span></a> &nbsp; |</li>
                    <li> <a href="javascript:void(0)" class="priceCalculator" id="priceCalculator"><i class="fa fa-calculator"></i> <span class="navHeadChatText">Price Calculator</span></a> &nbsp; |</li>
                    <li> <a href="mailto:enquiry@allassignmenthelp.com.au"><i class="fa fa-envelope"></i> Email Us</a> &nbsp; |</li>
                    <li class="phone"> <a href="refer-a-friend.html" rel="">Refer a Friend</a> &nbsp; | </li>
                    <li class="notification"><a href="#" class="alert"> <small class="bage">2</small>Notifications</a>
                        <div class="notification-list">
                            <ul>
                                <li>
                                    <a href="offers.html" class="clearfix">
                                        <figure><img src="~$imageUrl`/data/image_upload/15042457671496294264seasonal-offer.jpg" alt=""></figure>
                                        <span>
                                            <strong>Seasonal Offers: Avail 25% Discount on our Exclusive Academic Writing</strong>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="download-app.html" class="clearfix">
                                        <figure><img src="~$imageUrl`/data/image_upload/1481801509app-download.jpg" alt=""></figure>
                                        <span>
                                            <strong>Download our App and get extra 5% OFF on every order.</strong>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="search_site">
                            <form method="POST" action="" onsubmit="return validate_site_search(this);" id="search_site_form">
                                <input type="text" placeholder="Search" name="search_text" value="" id="site_search" class=""/>
                                <input type="hidden" name="sitesearch" value="1" >
                                <input class="hides-txt" name="search_sub" type="submit" >
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <header class="clearfix">
            <a href="index.html" class="logo"> <img src="~$imageUrl`/logo.png" alt="All Assignment Help Logo"></a>
            <div id="menuBtn">&#9776;</div>
            <nav id="myMenu">
                <ul>
                    <li class="service-menu"><a href="services.html" class="services-link">Services</a>
                        <div class="service-dropedown">
                            <ul class="service-dropedown-menu">
                                <li class="selected">
                                    <a class="new-toggle" >Assignment Subjects</a>
                                    <div class="service-dropedown-list clearfix">
                                        <ul>
                                            <li><a href="lawAssignment.php">Law</a></li>
                                            <li><a href="nursing-assignment-help.html">Nursing</a></li>
                                            <li><a href="economics-assignment-help.html">Economics</a></li>
                                            <li><a href="english-assignment-help.html">English</a></li>
                                            <li><a href="history-assignment-help.html">History</a></li>
                                            <li><a href="science-assignment-help.html">Science</a></li>
                                            <li><a href="psychology-assignment-help.html">Psychology</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="culture-assignment-help.html">Culture</a></li>
                                            <li><a href="fashion-assignment-help.html">Fashion</a></li>
                                            <li><a href="corporate-culture-assignment-help.html">Corporate Culture</a></li>
                                            <li><a href="mathematics-assignment-help.html">Mathematics</a></li>
                                            <li><a href="database-assignment-help.html">Database</a></li>
                                            <li><a href="geography-assignment-help.html">Geography</a></li>
                                            <li><a href="physics-assignment-help.html">Physics</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="probability-assignment-help.html">Probability</a></li>
                                            <li><a href="architecture-assignment-help.html">Architecture</a></li>
                                            <li><a href="arts-assignment-help.html">Arts</a></li>
                                            <li><a href="algebra-assignment-help.html">Algebra</a></li>
                                            <li><a href="anthropology-assignments-help.html">Anthropology</a></li>
                                            <li><a href="linux-assignment-help.html">Linux</a></li>
                                            <li><a href="botany-assignment-help.html">Botany</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a class="new-toggle" >Assignment Services</a>
                                    <div class="service-dropedown-list clearfix">
                                        <ul>
                                            <li><a href="do-my-assignment.html">Do My Assignment</a></li>
                                            <li><a href="coursework-help.html">Coursework Help</a></li>
                                            <li><a href="dissertation-help.html">Dissertation Help</a></li>
                                            <li><a href="how-to-write-assignment.html">Assignment Writing Help</a></li>
                                            <li><a href="university-assignment-help-australia.html">University Assignment</a></li>
                                            <li><a href="cheap-assignment-help.html">Cheap Assignments</a></li>
                                            <li><a href="college-assignment-help.html">College</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="resit-assignment-writing-service.html">Resit Assignment</a></li>
                                            <li><a href="mfrd-assignment-help.html">MFRD Assignment Help</a></li>
                                            <li><a href="media-assignment-help.html">Media Assignment Help</a></li>
                                            <li><a href="cheap-essay-writing.html">Cheap essay writing</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a class="new-toggle" >Management Subjects</a>
                                    <div class="service-dropedown-list clearfix">
                                        <ul>
                                            <li><a href="mba-assignment-help.html">MBA</a></li>
                                            <li><a href="accounting-assignment-help.html">Accounting</a></li>
                                            <li><a href="project-management-assignment-help.html">Project Management</a></li>
                                            <li><a href="marketing-assignment-help.html">Marketing</a></li>
                                            <li><a href="business-assignment-help.html">Business</a></li>
                                            <li><a href="tourism-assignment-help.html">Travel & Tourism</a></li>
                                            <li><a href="hr-assignment-help.html">HR Assignments</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="assignment-editing-and-proofreading-services.html">Assignment Editing proofreading</a></li>
                                            <li><a href="hospitality-assignment-help.html">Hospitality</a></li>
                                            <li><a href="taxation-assignment-help.html">Taxation</a></li>
                                            <li><a href="hotel-management-assignment.html">Hotel Management</a></li>
                                            <li><a href="performance-management-assignment.html">Performance Management</a></li>
                                            <li><a href="leadership-assignment-help.html">Leadership</a></li>
                                            <li><a href="risk-management-assignment.html">Risk Management</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="training-and-development-assignment.html">Training and Development</a></li>
                                            <li><a href="assignment-on-time-management.html">Assignment On Time Management</a></li>
                                            <li><a href="change-management-assignment-help.html">Change Management</a></li>
                                            <li><a href="consumer-behavior-assignment-help.html">Consumer Behavior</a></li>
                                            <li><a href="assignment-on-supply-chain-management.html">Supply Chain Management</a></li>
                                            <li><a href="managerial-accounting-assignment-help.html">Managerial Accounting</a></li>
                                            <li><a href="corporate-finance-assignment-help.html">Corporate Finance</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a class="new-toggle" >Engineering Subjects</a>
                                    <div class="service-dropedown-list clearfix">
                                        <ul>
                                            <li><a href="calculus-assignment-help.html">Calculus</a></li>
                                            <li><a href="chemical-engineering-assignment-help.html">Chemical Engineering</a></li>
                                            <li><a href="statistics-assignment-help.html">Statistics</a></li>
                                            <li><a href="ajax-assignment-help.html">Ajax</a></li>
                                            <li><a href="ruby-assignment-help.html">Ruby</a></li>
                                            <li><a href="javascript-assignment-help.html">JavaScript</a></li>
                                            <li><a href="asp-assignment-help.html">ASP.NET</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="android-assignment-help.html">Android</a></li>
                                            <li><a href="biochemical-engineering-assignment-help.html">Biochemical Engineering</a></li>
                                            <li><a href="computer-engineering-assignment.html">Computer Engineering</a></li>
                                            <li><a href="programming-assignment-help.html">Programming</a></li>
                                            <li><a href="telecommunications-engineering-assignment.html">Telecommunications Engineering</a></li>
                                            <li><a href="pascal-assignment-help.html">PASCAL</a></li>
                                            <li><a href="vlsi-assignment-help.html">VLSI</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="web-designing-assignment-help.html">Web Designing</a></li>
                                            <li><a href="aerospace-assignment-help.html">Aerospace</a></li>
                                            <li><a href="hydraulics-assignment-help.html">Hydraulics</a></li>
                                            <li><a href="analog-assignment-help.html">Analog</a></li>
                                            <li><a href="digital-electronics-assignment-help.html">Digital Electronics</a></li>
                                            <li><a href="artificial-intelligence-assignment-help.html">Artificial Intelligence</a></li>
                                            <li><a href="structural-engineering-assignment-help.html">Structural Engineering</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a class="new-toggle" >Essay Subjects</a>
                                    <div class="service-dropedown-list clearfix">
                                        <ul>
                                            <li><a href="essay-writing.html">Essay Writing</a></li>
                                            <li><a href="human-resource-essay-help.html">Human Resource</a></li>
                                            <li><a href="literature-essay-help.html">Literature Essay Writing</a></li>
                                            <li><a href="academic-essay-help.html">Academic Essay help</a></li>
                                            <li><a href="leadership-essay-writing.html">Leadership Essay Writing</a></li>
                                            <li><a href="admission-essay-writing.html">Admission Essay Help</a></li>
                                            <li><a href="write-my-essay.html">Write My Essay</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="exploratory-essay-writing.html">Exploratory Essay Writing</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a class="new-toggle" >Assignment By Cities</a>
                                    <div class="service-dropedown-list clearfix">
                                        <ul>
                                            <li><a href="assignment-help-sydney.html">Sydney</a></li>
                                            <li><a href="assignment-help-brisbane.html">Brisbane</a></li>
                                            <li><a href="assignment-help-melbourne.html">Melbourne</a></li>
                                            <li><a href="assignment-help-adelaide.html">Adelaide</a></li>
                                            <li><a href="assignment-help-gold-coast.html">Gold Coast</a></li>
                                            <li><a href="assignment-help-canberra.html">Canberra</a></li>
                                            <li><a href="assignment-help-townsville.html">Townsville</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="assignment-help-perth.html">Perth</a></li>
                                            <li><a href="assignment-help-gippsland.html">Gippsland</a></li>
                                            <li><a href="assignment-help-ballarat.html">Ballarat</a></li>
                                            <li><a href="assignment-help-bendigo.html">Bendigo</a></li>
                                            <li><a href="assignment-help-devonport.html">Devonport</a></li>
                                            <li><a href="assignment-help-cairns.html">Cairns</a></li>
                                            <li><a href="assignment-help-newcastle.html">Newcastle</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="assignment-help-coffs-harbour.html">Coffs Harbour</a></li>
                                            <li><a href="assignment-help-darwin.html">Darwin</a></li>
                                            <li><a href="assignment-help-geelong.html">Geelong</a></li>
                                            <li><a href="assignment-help-geraldton.html">Geraldton</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a class="new-toggle">Law Subjects</a>
                                    <div class="service-dropedown-list clearfix">
                                        <ul>
                                            <li><a href="criminal-law-assignment-help.html">Criminal Law Assignment Help</a></li>
                                            <li><a href="contract-law-assignment-help.html">Contract Law Assignment Help</a></li>
                                            <li><a href="business-law-assignment-help.html">Business Law Assignment Help</a></li>
                                            <li><a href="commercial-law-assignment-help.html">Commercial Law Assignment help</a></li>
                                            <li><a href="civil-law-assignment-help.html">Civil Law Assignment Help</a></li>
                                            <li><a href="corporate-law-assignment-help.html">Corporate Law Assignment Help</a></li>
                                            <li><a href="administrative-law-assignment-help.html">Administrative Law Assignment Help</a></li>
                                        </ul>
                                        <ul>
                                            <li><a href="property-law-assignment-help.html">Property Law Assignment Help</a></li>
                                            <li><a href="constitutional-law-assignment-help.html">Constitutional Law Assignment Help</a></li>
                                            <li><a href="international-law-assignment-help.html">International Law Assignment Help</a></li>
                                            <li><a href="tort-law-assignment-help.html">Tort Law</a></li>
                                            <li><a href="taxation-law-assignment-help.html">Taxation Law</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="submenu service-menu company-menu">
                        <a href="javascript:void(0)" class="our-company-menu">Company</a>
                        <div class="our-company">
                            <ul class="drop-down clearfix">
                                <li><a href="about.html">About us</a></li>
                                <li><a href="why-choose-us.html">Why choose us</a></li>
                                <li><a href="vision-and-mission.html">Mission & Vision </a></li>
                                <li><a href="guarantee.html">Guarantees</a></li>
                                <li><a href="samples.html">Sample</a></li>
                                <li><a href="reviews.html">Reviews </a></li>
                                <li><a href="blog.html"> Assignment Tips </a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="writers.html">Hire Writers</a></li>
                    <li><a href="how-it-works.html">How it works</a></li>
                    <li><a href="price.html">Pricing</a></li>
                    <li><a href="order-now.html">Order Now</a></li>
                    <li class="search-part">
                        <span class="ac">
                            <a class="mobs" href="customer_panel/index.html"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <a class="account" href="customer_panel/index.html" rel="nofollow" > Account</a>
                        </span>
                    </li>
                </ul>
            </nav>
            <div class="search-part in-mobile">
                <span>
                    <span class="ac">
                        <a class="mobs" href="customer_panel/index.html"><i class="fa fa-user" aria-hidden="true"></i></a>
                        <a class="account" href="customer_panel/index.html" rel="nofollow" > Account</a>
                    </span>
                </span>
            </div>
        </header>
    </div>
</div>
