<section class="your_service f-ban">
    <div class="container">
        <div class="border"></div>
        <h3>Call Us for Live Assistance</h3>
        <p>Now take our online assignment writing services in Australia.</p>
        <ul>
            <li><span data-mce-mark="1">Toll Free AU</span>+61 361 216 125 </li>
            <li><span data-mce-mark="1">Email Us</span>enquiry@allassignmenthelp.com.au </li>
        </ul>
        <a class="live_chat c_livechat ord" href="order-now.html">Order Now</a>
        <div class="clear"></div>
    </div>
</section>
<!-- footer top part End -->

<!-- footer Start -->
<footer class="clearfix">
    <div class="container">
        <section class="relatedlinks"></section>
        <section class="copyright">
            <article class="top-searches">
                <p class="mobile-foclick top-ser" id="6">Top Assignment Searches:</p>
                <div class="mobile-foclick-open" id="foclick6">
                    <a href="mba-assignment-help.html">MBA Assignment</a>
                    <a href="index.html">My Assignment Help</a>
                    <a href="coursework-help.html">Coursework</a>
                    <a href="dissertation-help.html">Dissertation Help</a>
                    <a href="essay-writing.html">Essay Writing</a>
                    <a href="cheap-assignment-help.html">Cheap Assignment help</a>
                    <a href="college-assignment-help.html">College Assignment Help</a>
                    <a href="resit-assignment-writing-service.html">Resit Assignment</a>
                    <a href="do-my-assignment.html">Do My Assignment</a>
                    <a href="blog.html">Blog</a>
                    <a href="reviews.html">Reviews</a>
                    <a href="accounting-assignment-help.html">Accounting Assignments</a>
                    <a href="samples.html">Assignment Samples</a>
                    <a href="assignment-help-sydney.html">Assignment Help Sydney </a>
                    <a href="assignment-help-melbourne.html">Melbourne Academic Help</a>
                    <a href="assignment-help-brisbane.html">Assignment Help Brisbane</a>
                    <a href="assignment-help-adelaide.html">Assignment Help Adelaide </a>
                    <a href="uk.html">UK</a>
                    <a href="usa.html">USA</a>
                    <a href="new-zealand.html">New Zealand</a>
                    <a href="malaysia.html">Malaysia</a>
                    <a href="uae.html">Assignment Help in UAE</a>
                    <a href="tort-law-assignment-help.html">Tort Law Assignments</a>
                    <a href="transportation-engineering-assignment-help.html">Transportation Engineering</a>
                </div>
            </article>
            <article class="client-base-bx clearfix">
                <p class="client-base"><strong>Client Base:</strong> Sydney, Brisbane, Queensland,Gold Coast, Adelaide, Melbourne, Townsville, Perth, South Australia,Victoria,Western Australia </p>
                <p>Disclaimer : All Assignment Help Australia Provides custom assignment writing services to facilitate college students. We offer references of reliable writing resources as well, to aid your learning process.</p>
                <div class="clearfix"></div>
                <div class="social-icon">
                    <ul>
                        <li><a rel="nofollow" title='Facebook' href="" target="_blank"><i class="facebook-sprite">&nbsp;</i></a></li>
                        <li><a title='Google' href="" target="_blank"><i class="google-sprite">&nbsp;</i></a></li>
                        <li><a title='Youtube' href=""><img src="~$imageUrl`/youtube.png" width="24" height="24" alt="youtube" /></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <span class="f-link">
                    <a href="refund-policy.html" rel="nofollow">Refund Policy</a> |
                    <a href="cancellation-policy.html" rel="nofollow">Cancellation Policy</a> |
                    <a href="terms-conditions.html" rel="nofollow">Terms & Conditions</a> |
                    <a href="privacy-policy.html" rel="nofollow">Privacy Policy</a> |
                    <a href="faq.html" rel="">FAQ</a>
                </span>
            </article>
        </section>
        <span class="copy">&copy; Copyright 2017 @ All Assignment Help Australia. All Rights Reserved </span>

        <div class="rating" itemscope itemtype="https://schema.org/Product">
            <span class="f-left" itemprop="name">Assignment Help </span>
            <div class="f-left" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating"> Rated <span itemprop="ratingValue">4.5</span>/5 based on <span itemprop="reviewCount">12942</span> reviews </div>
        </div>
        <div class="overlays"></div>
        <div class="overlays1"></div>
    </div>
    <a href="javascript:void(0);" class="zopim-chat" onclick="javascript:$zopim.livechat.window.show()"><img src="~$imageUrl`/chats.svg" alt="Live Chat"></a>

    <!--mobile app screen-->
    <div class="first-mobile-app">
        <span class="skip">Skip To Mobile Website</span>
    	<img src="~$imageUrl`/mobile-app-screen.jpg" alt="Download App">
        <a title="Download App" href="404.html">Download The App</a>
    </div>

    <!--end mobile app screen-->
    <div class="big-deals">
    	<span>Get the Biggest Deal Ever -  </span> Lowest guaranteed price across the globe. Get upto 50% off !    <a href='order-now.html'>Order Now</a>
    </div>
</footer>
<!-- footer End -->

<!-- contactus_modal Start Here -->
<div id="contactus_modal" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" class="close bClose" data-dismiss="modal" aria-hidden="true">×</a>
                <h3><i class="fa fa-phone">&nbsp;</i>&nbsp;Contact Support Team</h3>
                <p class="sub-header">Call or text us for support</p>
            </div>
            <div class="modal-body">
                <label class="valueLabel">United States Local (voice)</label>
                <div class="inputValue"><img src="~$imageUrl`/us.png" alt="US" title="US">&nbsp;+1-518-319-1344</div>

                <div class="clear15"></div>
                <label class="valueLabel">United Kingdom Local (voice)</label>
                <div class="inputValue"><img src="~$imageUrl`/gb.png" alt="UK" title="UK">&nbsp;+44-1202-560104</div>

                <div class="clear15"></div>
                <label class="valueLabel">Australia Local (voice)</label>
                <div class="inputValue"><img src="~$imageUrl`/au.png" alt="AU" title="AU">&nbsp;+61-3-6121-6125</div>

                <div class="clear15"></div>
                <label class="valueLabel"> WhatsApp (text)</label>
                <div class="inputValue">+44-759-645-8199</div>
            </div>
            <div class="modal-footer" style="margin-top: 50px;">
                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-check"></i>&nbsp;Ok
                </button>
            </div>
        </div>
    </div>
</div>
<!-- contactus_modal End Here -->

<!-- pricecalculator_modal Start Here -->
<div id="pricecalculator_modal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" class="close bClose" data-dismiss="modal" aria-hidden="true">×</a>
                <h3><i class="fa fa-usd">&nbsp;</i>&nbsp;Price Calculator</h3>
                <p class="sub-header">Find out how much your order will cost.</p>
            </div>
            <div class="modal-body">
                <div class="mainContent">
                    <input type="hidden" class="pcmCurrency" value="USD">
                    <input type="hidden" class="pcmDeadline" value="11">
                    <input type="hidden" class="pcmPage" value="1">
                    <div class="col-sm-12">
                        <div>
                            <div class="form-group">
                                <h5>Currency</h5>
                                <ul class="gajg dummyCurrency">
                                    <li class="gabd gabhb gabdp gal" data-attr="USD">USD</li>
                                    <li class="gabd gabhb gabdp " data-attr="GBP">GBP</li>
                                    <li class="gabd gabhb gabdp" data-attr="AUD">AUD</li>
                                    <li class="gabd gabhb gabdp" data-attr="SGD">SGD</li>
                                    <li class="gabd gabhb gabdp" data-attr="NZD">NZD</li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-12">
                        <div>
                            <div class="form-group">
                                <h5>Deadline</h5>
                                <ul class="gajg dummyDeadline">
                                    <li class="gabd gabhb gabdpi" data-attr="1">1 Day</li>
                                    <li class="gabd gabhb gabdpi" data-attr="2">2 Days</li>
                                    <li class="gabd gabhb gabdpi" data-attr="3">3 Days</li>
                                    <li class="gabd gabhb gabdpi" data-attr="4">4-6 Days</li>
                                    <li class="gabd gabhb gabdpi" data-attr="7">7-10 Days</li>
                                    <li class="gabd gabhb gabdpi gal" data-attr="11">&gt; 10 Days</li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-12">
                        <div>
                            <div class="form-group">
                                <h5>Pages</h5>
                                <div id="contentspinm" class="contentspinm"><button class="dec buttonspin" title="Decrease">-</button><input type="text" class="pageQtym text-center" value="1" maxlength="4"><button class="inc buttonspin" title="Increase">+</button></div>
                                &nbsp;&nbsp;<span class="pcWordCountm">250 </span> Words
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-12">
                        <div>
                            <div class="pb10">
                                <h2 class="totalPricem"><span class="green">12 USD</span></h2>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="loader" style="display:none;">Loading...</div>
            </div>
            <div class="modal-footer">
                <div class="modal-ftr-left"><p style="text-align: left;font-size: 11px;line-height:16px;padding-bottom: 22px;">Price shown is approximate. Actual price depends on 1) Type of Work 2) Number of Pages 3) Urgency 4) Academic Level</p></div>
                <div class="modal-ftr-right"><a href="javascript:void(0);" onclick="lh_inst.lh_openchatWindow();$('#pricecalculator_modal').modal('hide');">Chat with Us</a>&nbsp;&nbsp;<a href="https://www.allassignmenthelp.com/order" class="btn btn-primary">Order Now</a></div>
            </div>
        </div>
    </div>
</div>
<!-- pricecalculator_modal End Here -->

<script src="~$jsUrl`/jquery-1.11.3.min.js"></script>
<script src="~$jsUrl`/jquery.bpopup.min.js"></script>
<script src="~$jsUrl`/jquery.easing.1.3.js"></script>
<script src="~$jsUrl`/custommain.js"></script>

<script type="text/javascript">

    $(".skip").click(function(){
    var root_url = 'index.html';
    $(".first-mobile-app").hide();
    var d = new Date();
    d.setTime(d.getTime() + 24 * 1 * 60 * 60 * 1e3);
    var e = "expires=" + d.toGMTString();
    document.cookie = "skip_mobile=true;" + e;
    // window.location = root_url;
    });
</script>
<script>
	$(".notification").click(function(){
        $(".notification-list").slideToggle(400);
    });

    if ($(window).width() <= 1024) {
        $(".our-company-menu").click(function(){
            $(".our-company").slideToggle(200);
        });
    }
	$(".skip").click(function(){
        $(".first-mobile-app").hide();
    });

</script>

<script>
    // Start ipad Jquery
    if ($(window).width() <= 1024) {
        $(".services-link").click(function(){
            $(".service-dropedown").toggle();
            return false;
        });
        $(".service-dropedown").click(function(e){
            e.stopPropagation();
        });
        $(document).click(function(){
            $(".service-dropedown").hide();
        });
    }
    /*home-services toggle for small devices*/

    ////// services drop down menu
    (function ($) {
    if ($(window).width() <= 767) {
    $(".menu").slideUp();
    var allPanels = $('.service-dropedown-menu .service-dropedown-list').hide();
    $('.service-dropedown-menu li .new-toggle').click(function () {
    var ss = 0;
    if ($(this).next('.service-dropedown-list').is(':visible'))
    {
    ss = 1;
    }
    allPanels.slideUp();
    if (ss == 0)
    {
    $(this).next('.service-dropedown-list').slideDown();
    }
    return false;
    });
    }
    })(jQuery);

    //our-company dropdown menu
</script>
<script>
    ;(function($) {
        $(function() {
            $('#phoneSupport').bind('click', function(e) {
                e.preventDefault();
                $('#contactus_modal').bPopup({
                    easing: 'easeOutBack', //uses jQuery easing plugin
                    speed: 450,
                    transition: 'slideDown',
                    modalClose: true
                });
            });
            $('#priceCalculator').bind('click', function(e) {
                e.preventDefault();
                $('#pricecalculator_modal').bPopup({
                    easing: 'easeOutBack', //uses jQuery easing plugin
                    speed: 450,
                    transition: 'slideDown',
                    modalClose: true
                });
            });

        });

    })(jQuery);

</script>

</body>
</html>
