<?php
//error_reporting(1);
ini_set('display_errors','1');
session_start();
date_default_timezone_set('Europe/London');
$now = new DateTime();
$mins = $now->getOffset() / 60;
$sgn = ($mins < 0 ? -1 : 1);
$mins = abs($mins);
$hrs = floor($mins / 60);
$mins -= $hrs * 60;
$offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
$domain = "http://prashant.com/aahaus";
$folderName = "/live";
$webRoot = "";
$rootDir = "/var/www/html".$folderName;
define('OFFSET', $offset);
define('COOKIE_NAME', 'globalusersuperman');
define('COOKIE_DOMAIN', '.prashant.com');
define('COOKIE_BROWSER', 'checkmybrowsersuperman');
define('ENCRYPTION_KEY','gL0b@l@ss!gnmenT$eRv!cEs');

require_once($rootDir.'/lib/common/Mobile_Detect.php');
$detect = new Mobile_Detect();
if ( $detect->isMobile() || $detect->isTablet() ) {
	$isMobile = 1;
} else {
	$isMobile = 0;
}

require_once($rootDir.'/lib/common/ParentManagerFactory.class.php');
require_once($rootDir.'/lib/common/Encryption.class.php');
require_once($rootDir.'/lib/common/Validator.class.php');
require_once($rootDir.'/lib/common/Internationalization.class.php');
require_once($rootDir.'/lib/common/CommonFunc.class.php');
require_once($rootDir.'/lib/common/InternalMail.class.php');
require_once($rootDir.'/lib/common/CommonMail.class.php');
require_once($rootDir.'/lib/common/PasswordHash.php');
require_once($rootDir.'/lib/common/NexmoApi.php');
require_once($rootDir.'/lib/common/AuthenticateSuperman.class.php');
require_once($rootDir.'/plugins/Smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->setCompileDir($rootDir.'/plugins/superman/smarty/templates_c');
$smarty->setCacheDir($rootDir.'/plugins/superman/smarty/cache');
$smarty->setConfigDir($rootDir.'/plugins/superman/smarty/configs');
$smarty->setTemplateDir(dirname(__FILE__).'/templates');

require_once($rootDir.'/lib/common/KLogger.php');
global $log;
$log   = KLogger::instance($rootDir.'/login-logs/', KLogger::DEBUG);
$objTrackIpManager = ParentManagerFactory::getInstance()->getTrackIpManager();
$objAuthenticateSuperman = AuthenticateSuperman::getInstance();
$smarty->assign('imageUrl', $domain.$webRoot.'/static/images');
$smarty->assign('cssUrl', $domain.$webRoot.'/static/css');
$smarty->assign('jsUrl', $domain.$webRoot.'/static/js');
$smarty->assign('domainUrl', $domain);
$smarty->assign('folderName',$folderName);
$smarty->assign('rootDir',$rootDir);
$smarty->assign('webRoot',$webRoot);
$smarty->assign('cssVersion','141220141');
$smarty->assign('jsVersion','17122015');
$smarty->assign('isMobile', $isMobile);
